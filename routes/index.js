"use strict";

var Todo = require("../model/todo").Todo;

exports.index = function(req, res, next){
  Todo.find()
      .sort({createdAt: 1})
      .exec(function(err, todos) {
        if (!err){
          console.log(todos);
          res.render('index', { todos: JSON.stringify(todos)} );
        } else {
          next(err)
        }
      })
};