"use strict";

var Todo = require("../model/todo").Todo
  , app = require("../app").app
  , routes = require('../routes');


app.get('/', routes.index);