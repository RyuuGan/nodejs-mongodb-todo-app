"use strict";

var app = require("../app").app
  , mongoose = require("mongoose")
  , ObjectId = mongoose.Types.ObjectId
  , Todo = require("../model/todo").Todo;


app.get("/api/todo/list", function(req, res, next){
  Todo.find().sort({createdAt: 1}).exec(function(err, todos){
    if (!err){
      res.json(todos)
    } else {
      next(err)
    }
  });
});

app.put("/api/todo", function(req, res, next){
  var t = new Todo();
  var title = req.param("title");
  if (title && title.trim() != "") {
    t.title = title;
    t.save();
    res.json(t);
  } else {
    res.json('"errors": ["Please, specify title"]');
  }
});

app.post("/api/todo/:id", function(req, res, next){
  var id = new ObjectId(req.param("id"));
  Todo.findById(id).exec(function(err, todo){
    if (todo) {
      todo.completed = (req.param("completed") || false);
      var title = req.param("title");
      if (title && title.trim() != "") {
        todo.title = title;
        todo.save();
        res.json(todo);
      } else {
        res.json('"errors": ["Please, specify title"]');
      }
    } else {
      res.status(404).send("Not found.");
      // We can even render
      // res.status(404).render('error404')
    }

  })
});