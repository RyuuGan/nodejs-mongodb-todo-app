"use strict";

var mongoose = require("mongoose");

var todoSchema = new mongoose.Schema({
  title: String,
  completed: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: new Date().toISOString()
  }
});

var Todo = mongoose.model('Todo', todoSchema);

exports.Todo = Todo;