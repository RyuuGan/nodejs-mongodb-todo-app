"use strict";

function TodoCtrl($scope, $http) {
  $scope.todos = [];
  function updateTodoList() {
    $http.get("/api/todo/list").success(function(data){
      $scope.todos = data;
    });
  }
  updateTodoList();

  $scope.createTodo = function() {
    var title = $scope.todoTitle;
    if (title && title.trim() != "") {
      var todo = {
        title: title
      };
      $http.put('/api/todo', todo).success(function(res) {
        if (res.errors) {
          updateTodoList();
        } else {
          todo._id = res._id;
          todo.completed = res.completed;
          todo.createdAt = res.createdAt;
          $scope.todos.push(todo);
        }
      }).error(function(){
            updateTodoList();
          });
      $scope.todoTitle = '';
    }
  };

  $scope.remaining = function() {
    var count = 0;
    angular.forEach($scope.todos, function(todo) {
      count += todo.completed ? 0 : 1;
    });
    return count;
  };

  $scope.editTodo = function(idx){
    var todo = $scope.todos[idx];
    if (todo) {
      $http.post("/api/todo/" + todo._id, todo).success(function(res){
        if (res.errors) {
          updateTodoList();
        }
      });
    }
  }
}