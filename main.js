"use strict";

var http = require('http')
  , app = require("./app").app;

require("./routes/todo");
require("./routes/todo_api");

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
